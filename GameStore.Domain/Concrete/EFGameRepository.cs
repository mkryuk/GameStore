﻿using System;
using System.Collections.Generic;
using GameStore.Domain.Abstract;
using GameStore.Domain.Entities;

namespace GameStore.Domain.Concrete
{
    public class EFGameRepository : IGameRepository
    {
        EFDbContext context = new EFDbContext();
        public IEnumerable<Game> Games
        {
            get { return context.Games; }
        }

        public void SaveGame(Game game)
        {           
            var item = context.Games.Find(game.GameId);
            if (item != null)
            {
                item.Name = game.Name;
                item.Description = game.Description;
                item.Price = game.Price;
                item.Category = game.Category;
            }
            else
            {
                context.Games.Add(game);
            }

            context.SaveChanges();
        }

        public Game DeleteGame(int gameId)
        {
            var item = context.Games.Find(gameId);
            if (item != null)
            {
                context.Games.Remove(item);
                context.SaveChanges();
            }
            return item;
        }
    }
}
