﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.Domain.Entities
{
    public class ShippingDetails
    {
        [Required(ErrorMessage = "Enter your name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Enter shipping address")]
        [Display(Name = "Address 1")]
        public string Line1 { get; set; }
        [Display(Name = "Address 2")]
        public string Line2 { get; set; }
        [Display(Name = "Address 3")]
        public string Line3 { get; set; }

        [Required(ErrorMessage = "Enter your city")]
        public string City { get; set; }

        [Required(ErrorMessage = "Enter your country")]
        public string Country { get; set; }

        public bool GiftWrap { get; set; }
    }
}
