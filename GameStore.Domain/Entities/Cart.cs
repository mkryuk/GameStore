﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.Domain.Entities
{
    public class CartLine
    {
        public Game Game { get; set; }
        public int Quantity { get; set; }
    }

    public class Cart
    {
        private List<CartLine> lineCollection = new List<CartLine>();
        public List<CartLine> Lines {
            get { return lineCollection; }
        }

        // Add item to the cart
        public void AddItem(Game game, int quantity)
        {
            var line = lineCollection
                        .FirstOrDefault(item => item.Game.GameId == game.GameId);
            if (line == null)
            {
                lineCollection.Add(new CartLine {Game = game, Quantity = quantity});
            }
            else
            {
                line.Quantity += quantity;
            }
        }

        // Removes item from the cart
        public void RemoveItem(Game game)
        {
            lineCollection.RemoveAll(item => item.Game.GameId == game.GameId);
        }

        // Calculate total price
        public decimal ComputeTotalPrice()
        {
            return lineCollection.Sum(item => item.Game.Price*item.Quantity);
        }

        // Clear the cart

        public void Clear()
        {
            lineCollection.Clear();
        }
    }
}
