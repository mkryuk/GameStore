﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GameStore.WebUI.Infrastructure;
using GameStore.WebUI.Models;

namespace GameStore.WebUI.Controllers
{
    public class AccountController : Controller
    {
        private IAuthProvider authProvider;
        public AccountController(IAuthProvider provider)
        {
            authProvider = provider;
        }
        // GET: Account
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginViewModel credentials, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                if (authProvider.Authenticate(credentials.UserName, credentials.Password))
                {
                    return Redirect(returnUrl ?? Url.Action("Index", "Admin"));
                }

                ModelState.AddModelError("", "Credentials are invalid");                
            }
            return View();
        }
    }
}