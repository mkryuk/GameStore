﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GameStore.Domain.Abstract;
using GameStore.Domain.Entities;

namespace GameStore.WebUI.Controllers
{
    [Authorize]
    public class AdminController : Controller
    {
        private IGameRepository repository;

        public AdminController(IGameRepository repo)
        {
            repository = repo;
        }
        // GET: Admin
        public ViewResult Index()
        {
            return View(repository.Games);
        }

        public ViewResult Edit(int gameId)
        {
            var game = repository.Games
                .FirstOrDefault(g => g.GameId == gameId);
            return View(game);
        }

        [HttpPost]
        public ActionResult Edit(Game game)
        {
            if (!ModelState.IsValid) return View(game);

            repository.SaveGame(game);
            TempData["message"] = $"Changes in game {game.Name} has been saved";
            return RedirectToAction("Index");
        }

        public ViewResult Create()
        {
            return View("Edit", new Game());
        }

        [HttpPost]
        public ActionResult Delete(int gameId)
        {
            var game = repository.DeleteGame(gameId);
            if (game != null)
            {
                TempData["message"] = $"Game {game.Name} deleted";
            }
            return RedirectToAction("Index");
        }
    }
}