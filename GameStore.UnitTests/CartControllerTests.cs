﻿using System.Collections.Generic;
using System.Linq;
using GameStore.Domain.Abstract;
using GameStore.Domain.Entities;
using GameStore.WebUI.Controllers;
using GameStore.WebUI.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace GameStore.UnitTests
{
    [TestClass]
    public class CartControllerTests
    {
        [TestMethod]
        public void CanAddGameToCart()
        {
            var mock = new Mock<IGameRepository>();
            mock.Setup(m => m.Games)
                .Returns(new List<Game>
                {
                    new Game { GameId = 1, Name = "Game1", Category = "Cat1" }
                });

            var cart = new Cart();
            var controller = new CartController(mock.Object, null);
            controller.AddToCart(cart, 1, null);

            Assert.AreEqual(cart.Lines.Count(), 1);
            Assert.AreEqual(cart.Lines.ToList()[0].Game.GameId, 1);
        }

        [TestMethod]
        public void AddingGameToCartLeadsToCartScreen()
        {
            var mock = new Mock<IGameRepository>();
            mock.Setup(m => m.Games)
                .Returns(new List<Game>
                {
                    new Game { GameId = 1, Name = "Game1", Category = "Cat1" }
                });

            var cart = new Cart();
            var controller = new CartController(mock.Object, null);
            var result = controller.AddToCart(cart, 1, "myUrl");

            Assert.AreEqual(result.RouteValues["action"], "Index");
            Assert.AreEqual(result.RouteValues["returnUrl"], "myUrl");
        }

        [TestMethod]
        public void CanRemoveGameFromCart()
        {
            var mock = new Mock<IGameRepository>();
            mock.Setup(m => m.Games)
                .Returns(new List<Game>
                {
                    new Game { GameId = 1, Name = "Game1", Category = "Cat1" },
                    new Game { GameId = 2, Name = "Game2", Category = "Cat2" },
                    new Game { GameId = 3, Name = "Game3", Category = "Cat2" }
                });

            var cart = new Cart();
            var controller = new CartController(mock.Object, null);

            controller.AddToCart(cart, 1, null);
            controller.AddToCart(cart, 2, null);
            controller.AddToCart(cart, 3, null);

            controller.RemoveFromCart(cart, 2, null);

            Assert.AreEqual(cart.Lines.Count(), 2);
            Assert.AreEqual(cart.Lines.ToList()[0].Game.GameId, 1);
            Assert.AreEqual(cart.Lines.ToList()[1].Game.GameId, 3);
        }

        [TestMethod]
        public void RemovingGameFromCartLeadsToCartScreen()
        {
            var mock = new Mock<IGameRepository>();
            mock.Setup(m => m.Games)
                .Returns(new List<Game>
                {
                    new Game { GameId = 1, Name = "Game1", Category = "Cat1" }
                });

            var cart = new Cart();
            var controller = new CartController(mock.Object, null);
            var result = controller.RemoveFromCart(cart, 1, "myUrl");

            Assert.AreEqual(result.RouteValues["action"], "Index");
            Assert.AreEqual(result.RouteValues["returnUrl"], "myUrl");
        }

        [TestMethod]
        public void CanViewCartContent()
        {
           
            var cart = new Cart();
            var controller = new CartController(null, null);
            var result = (CartIndexViewModel)controller.Index(cart, "myUrl").Model;

            Assert.AreSame(cart, result.Cart);
            Assert.AreEqual(result.ReturnUrl, "myUrl");
        }
    }
}
