﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using GameStore.WebUI.Controllers;
using GameStore.WebUI.Infrastructure;
using GameStore.WebUI.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace GameStore.UnitTests
{
    [TestClass]
    public class AccountControllerTests
    {
        [TestMethod]
        public void CanLoginWithValidCredentials()
        {
            var mock = new Mock<IAuthProvider>();
            mock.Setup(m => m.Authenticate("admin", "123"))
                .Returns(true);
            var model = new LoginViewModel()
            {
                UserName = "admin",
                Password = "123"
            };

            var controller = new AccountController(mock.Object);
            var result = controller.Login(model, "/MyUrl");

            Assert.IsInstanceOfType(result, typeof (RedirectResult));
            Assert.AreEqual("/MyUrl", ((RedirectResult) result).Url);
        }

        [TestMethod]
        public void CanNOTLoginWithINVALIDCredentials()
        {
            var mock = new Mock<IAuthProvider>();
            mock.Setup(m => m.Authenticate("admin", "123"))
                .Returns(true);
            var model = new LoginViewModel()
            {
                UserName = "badUser",
                Password = "badPassword"
            };

            var controller = new AccountController(mock.Object);
            var result = controller.Login(model, "/MyUrl");

            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.IsFalse(((ViewResult)result).ViewData.ModelState.IsValid);
        }
    }
}
