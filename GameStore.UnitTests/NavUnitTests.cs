﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameStore.Domain.Abstract;
using GameStore.Domain.Entities;
using GameStore.WebUI.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace GameStore.UnitTests
{
    [TestClass]
    public class NavUnitTests
    {
        [TestMethod]
        public void CanCreateCategories()
        {
            var mock = new Mock<IGameRepository>();
            mock.Setup(m => m.Games)
                .Returns(new List<Game>
                {
                    new Game {GameId = 1, Name = "Game1", Category = "Cat1" },
                    new Game {GameId = 2, Name = "Game2", Category = "Cat2" },
                    new Game {GameId = 3, Name = "Game3", Category = "Cat1" },
                    new Game {GameId = 4, Name = "Game4", Category = "Cat3" },
                    new Game {GameId = 5, Name = "Game5", Category = "Cat2" }
                });

            var controller = new NavController(mock.Object);

            var result = ((IEnumerable<string>) controller.Menu().Model).ToList();

            Assert.AreEqual(result.Count(), 3);
            Assert.AreEqual(result[0], "Cat1");
            Assert.AreEqual(result[1], "Cat2");
            Assert.AreEqual(result[2], "Cat3");
        }

        [TestMethod]
        public void IndicateSelectedCategory()
        {
            var mock = new Mock<IGameRepository>();
            mock.Setup(m => m.Games)
                .Returns(new List<Game>
                {
                    new Game {GameId = 1, Name = "Game1", Category = "Cat1" },
                    new Game {GameId = 2, Name = "Game2", Category = "Cat2" },
                    new Game {GameId = 3, Name = "Game3", Category = "Cat1" },
                    new Game {GameId = 4, Name = "Game4", Category = "Cat3" },
                    new Game {GameId = 5, Name = "Game5", Category = "Cat2" }
                });

            var controller = new NavController(mock.Object);
            var selectedCategory = "Cat1";

            string result = controller.Menu(selectedCategory).ViewBag.SelectedCategory;

            Assert.AreEqual(selectedCategory, result);

        }

    }
}
