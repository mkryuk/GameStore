﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameStore.Domain.Abstract;
using GameStore.Domain.Entities;
using GameStore.WebUI.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace GameStore.UnitTests
{
    [TestClass]
    public class AdminControllerTests
    {
        [TestMethod]
        public void IndexDisplaysAllGames()
        {
            var mock = new Mock<IGameRepository>();
            mock.Setup(m => m.Games)
                .Returns(new List<Game>
                {
                    new Game {GameId = 1, Name = "Game1" },
                    new Game {GameId = 2, Name = "Game2" },
                    new Game {GameId = 3, Name = "Game3" },
                    new Game {GameId = 4, Name = "Game4" }
                });

            var controller = new AdminController(mock.Object);
            var result = ((IEnumerable<Game>)controller.Index().Model).ToList();

            Assert.AreEqual(result.Count(), 4);
            Assert.AreEqual(result[0].Name, "Game1");
            Assert.AreEqual(result[1].Name, "Game2");
            Assert.AreEqual(result[2].Name, "Game3");
            Assert.AreEqual(result[3].Name, "Game4");
        }

        [TestMethod]
        public void CanEditGame()
        {
            var mock = new Mock<IGameRepository>();
            mock.Setup(m => m.Games)
                .Returns(new List<Game>
                {
                    new Game {GameId = 1, Name = "Game1" },
                    new Game {GameId = 2, Name = "Game2" },
                    new Game {GameId = 3, Name = "Game3" },
                    new Game {GameId = 4, Name = "Game4" }
                });

            var controller = new AdminController(mock.Object);
            var game1 = controller.Edit(1).Model as Game;
            var game2 = controller.Edit(2).Model as Game;
            var game3 = controller.Edit(3).Model as Game;
            var game4 = controller.Edit(4).Model as Game;

            Assert.AreEqual(game1.GameId, 1);
            Assert.AreEqual(game2.GameId, 2);
            Assert.AreEqual(game3.GameId, 3);
            Assert.AreEqual(game4.GameId, 4);
        }

        [TestMethod]
        public void CannotEditNonexistingGame()
        {
            var mock = new Mock<IGameRepository>();
            mock.Setup(m => m.Games)
                .Returns(new List<Game>
                {
                    new Game {GameId = 1, Name = "Game1" },
                    new Game {GameId = 2, Name = "Game2" },
                    new Game {GameId = 3, Name = "Game3" },
                    new Game {GameId = 4, Name = "Game4" }
                });

            var controller = new AdminController(mock.Object);
            var game = controller.Edit(42).Model as Game;

            Assert.IsNull(game);
        }

        [TestMethod]
        public void CanDeleteGame()
        {
            var mock = new Mock<IGameRepository>();
            mock.Setup(m => m.Games)
                .Returns(new List<Game>
                {
                    new Game {GameId = 1, Name = "Game1" },
                    new Game {GameId = 2, Name = "Game2" },
                    new Game {GameId = 3, Name = "Game3" },
                    new Game {GameId = 4, Name = "Game4" }
                });

            var controller = new AdminController(mock.Object);
            controller.Delete(3);
            mock.Verify(m => m.DeleteGame(3));
        }
    }
}
