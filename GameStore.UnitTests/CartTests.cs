﻿using System.Linq;
using GameStore.Domain.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GameStore.UnitTests
{
    [TestClass]
    public class CartTests
    {
        [TestMethod]
        public void CanAddNewLines()
        {
            var game1 = new Game { GameId = 1, Name = "Game1" };
            var game2 = new Game { GameId = 2, Name = "Game2" };

            var cart = new Cart();

            cart.AddItem(game1, 1);
            cart.AddItem(game2, 1);

            var result = cart.Lines.ToList();

            Assert.AreEqual(result.Count, 2);
            Assert.AreEqual(result[0].Game, game1);
            Assert.AreEqual(result[1].Game, game2);
        }
        /// <summary>
        /// Check that we can add items for existing lines
        /// </summary>
        [TestMethod]
        public void CanAddQuantityForExistingLines()
        {
            var game1 = new Game { GameId = 1, Name = "Game1" };
            var game2 = new Game { GameId = 2, Name = "Game2" };

            var cart = new Cart();

            cart.AddItem(game1, 1);
            cart.AddItem(game2, 1);
            cart.AddItem(game1, 3);

            var result = cart.Lines.ToList();

            Assert.AreEqual(result.Count, 2);
            Assert.AreEqual(result[0].Quantity, 4);
            Assert.AreEqual(result[1].Quantity, 1);
        }

        [TestMethod]
        public void CanRemoveLine()
        {
            var game1 = new Game { GameId = 1, Name = "Game1" };
            var game2 = new Game { GameId = 2, Name = "Game2" };
            var game3 = new Game { GameId = 3, Name = "Game3" };

            var cart = new Cart();

            cart.AddItem(game1, 1);
            cart.AddItem(game2, 1);
            cart.AddItem(game3, 3);
            cart.AddItem(game2, 2);

            cart.RemoveItem(game2);
            var result = cart.Lines.ToList();
            Assert.AreEqual(result.Count(item => item.Game.GameId == game2.GameId), 0);
            Assert.AreEqual(result.Count(), 2);
            Assert.AreEqual(result[0].Game.Name, "Game1");
            Assert.AreEqual(result[1].Game.Name, "Game3");
        }

        [TestMethod]
        public void CanComputeTotalPrice()
        {
            var game1 = new Game { GameId = 1, Name = "Game1", Price = 100 };
            var game2 = new Game { GameId = 2, Name = "Game2", Price = 175 };
            var game3 = new Game { GameId = 3, Name = "Game3", Price = 230 };
            //Total price is 735

            var cart = new Cart();

            cart.AddItem(game1, 1);
            cart.AddItem(game2, 1);
            cart.AddItem(game3, 2);

            var result = cart.ComputeTotalPrice();

            Assert.AreEqual(result, 735);
        }

        [TestMethod]
        public void CanClearCart()
        {
            var game1 = new Game { GameId = 1, Name = "Game1" };
            var game2 = new Game { GameId = 2, Name = "Game2" };
            var game3 = new Game { GameId = 3, Name = "Game3" };
            

            var cart = new Cart();

            cart.AddItem(game1, 1);
            cart.AddItem(game2, 1);
            cart.AddItem(game3, 2);
            cart.Clear();

            Assert.AreEqual(cart.Lines.Count(), 0);
        }
    }
}
